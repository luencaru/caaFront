// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-good-table/dist/vue-good-table.css'
import VueGoodTable from 'vue-good-table';

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VueGoodTable);

new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})